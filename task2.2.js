function superSort(arr) {
  let relations = {};
  let arrWithoutNumbers = [];
  for (let i in arr) {
    let item = arr[i];
    let withoutNumbers = item.replace(/\d+/g, "");
    relations[withoutNumbers] = item;
    arrWithoutNumbers.push(withoutNumbers);
  }
  arrWithoutNumbers.sort();
  let arrFullSorted = [];
  for (let i in arrWithoutNumbers) {
    let item = arrWithoutNumbers[i];
    arrFullSorted.push(relations[item]);
  }
  return arrFullSorted;
}

console.log(superSort(["mic09ha1el", "4b5en6", "michelle", "be4atr3ice"]));
