function getDaysForMonth(month) {
  switch (month) {
    case 1:
      days = "31";
      break;
    case 2:
      days = "28";
      break;
    case 3:
      days = "31";
      break;
    case 4:
      days = "30";
      break;
    case 5:
      days = "31";
      break;
    case 6:
      days = "30";
      break;
    case 7:
      days = "31";
    case 8:
      days = "31";
      break;
    case 9:
      days = "30";
      break;
    case 10:
      days = "31";
      break;
    case 11:
      days = "30";
      break;
    case 12:
      days = "31";
  }
  return days;
}

for (let month = 1; month <= 12; month++) {
  console.log(getDaysForMonth(month));
}
