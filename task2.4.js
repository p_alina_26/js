function findUnique(array) {
  let lastSeen = null;

  for (let i = 0; i < array.length; i++) {
    let element = array[i];
    if (lastSeen == null) {
      lastSeen = element;
      continue;
    }

    if (lastSeen != element) {
      if (i == array.length - 1) {
        // last element of array, so it's unique ones
        return element;
      } else {
        if (array[i + 1] != element) {
          return element;
        } else {
          return lastSeen;
        }
      }
    }
  }
}

console.log(findUnique([1, 2, 1]));
console.log(findUnique([2, 1, 1]));
console.log(findUnique([1, 1, 2]));
