const API_URL = "https://api.github.com/";
const rootElement = document.getElementById("root");
const loadingElement = document.getElementById("loading-overlay");
const fightersDetailsMap = new Map();

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    method,
  };

  return fetch(url, options)
    .then((response) =>
      response.ok ? response.json() : Promise.reject(Error("Failed to load"))
    )
    .catch((error) => {
      throw error;
    });
}

class FighterService {
  async getFighters() {
    try {
      // https://github.com/binary-studio-academy/stage-2-es6-for-everyone/blob/master/resources/api/fighters.json
      const endpoint =
        "repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/fighters.json";
      const apiResult = await callApi(endpoint, "GET");

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      // https://github.com/binary-studio-academy/stage-2-es6-for-everyone/blob/master/resources/api/details/fighter/1.json
      const endpoint =
        "repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/details/fighter/" +
        id +
        ".json";
      const apiResult = await callApi(endpoint, "GET");

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }
}

const fighterService = new FighterService();

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Fighter {
  constructor(name, attack, defense, health, source, id) {
    this.name = name;
    this.attack = attack;
    this.defense = defense;
    this.health = health;
    this.source = source;
    this.id = id;
  }

  getHitPower() {
    let criticalHitChance = getRandomIntInclusive(1, 2);
    return this.attack * criticalHitChance;
  }

  getBlockPower() {
    let dodgeChance = getRandomIntInclusive(1, 2);
    return this.defense * dodgeChance;
  }
}

class View {
  element;

  createElement({ tagName, className = "", attributes = {} }) {
    const element = document.createElement(tagName);
    element.classList.add(className);

    Object.keys(attributes).forEach((key) =>
      element.setAttribute(key, attributes[key])
    );

    return element;
  }
}
class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);

    this.element = this.createElement({ tagName: "div", className: "fighter" });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener(
      "click",
      (event) => handleClick(event, fighter),
      false
    );
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name",
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes,
    });

    return imgElement;
  }
}

class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map((fighter) => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters",
    });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter);
    console.log("clicked");
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}
class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById("root");
  static loadingElement = document.getElementById("loading-overlay");

  async startApp() {
    try {
      App.loadingElement.style.visibility = "visible";

      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      fighters.forEach(async (f) => {
        let fighterDetails = await fighterService.getFighterDetails(f["_id"]);
        console.log(fighterDetails);

        let fighter = new Fighter(
          fighterDetails["name"],
          fighterDetails["attack"],
          fighterDetails["defense"],
          fighterDetails["health"],
          fighterDetails["source"],
          fighterDetails["_id"]
        );
        console.log(fighter.getHitPower(), fighter.getBlockPower());
      });

      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = "Failed to load data";
    } finally {
      App.loadingElement.style.visibility = "hidden";
    }
  }
}

new App();
