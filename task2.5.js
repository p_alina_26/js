function getDiscount(number) {
  let price = 10;
  if (number < 5) {
    discount = 1;
  } else if (number >= 5 && number < 10) {
    discount = 0.95;
  } else {
    discount = 0.9;
  }
  return number * price * discount;
}

console.log(getDiscount(4));
console.log(getDiscount(5));
console.log(getDiscount(9));
console.log(getDiscount(10));
console.log(getDiscount(11));
